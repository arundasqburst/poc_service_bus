'use strict';
const mongoose = require('mongoose');

class MongoConnection {

    // Connect to mongo db
    
    constructor() {
        this.mongoUrl = 'mongodb://arundas:qburst123@ds263380.mlab.com:63380/course_app_sub';
    }

    connectDb() {
        mongoose.connect(this.mongoUrl, (error, db) => {
            if (error) {
                console.log('Cannot establish database connection...');
            } else {
                console.log('Connection Established...');
            }
        });
    }

}

module.exports = MongoConnection;
