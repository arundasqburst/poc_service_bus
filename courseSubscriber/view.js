const bus = require('servicebus').bus();
const Course = require('./models/course')

class CourseView {
    
    listenToBus() {
        const that = this;
        bus.listen('event.courseCreator', function (event) {
            that.createNewCourse(event);
        });
    }
    async getAllCourses() {
        const courses = await Course.find();
        return courses;
    }
    async createNewCourse(reqData) {
        Course.create(reqData.course);
    }
}

module.exports = CourseView;
