const MongoConnection = require('./dbConnecter')
const Router = require('./route');

const dbConnection = new MongoConnection();
dbConnection.connectDb();

const routes = new Router();
routes.routeConnections();
