var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = new Schema({
    name: {type: String, unique: true, required: true},
    description: {type: String},
});

module.exports = mongoose.model('Course', schema);
