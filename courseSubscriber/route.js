const http = require('http');

const CourseView = require('./view');

class Router {

    constructor() {
        this.courseView = new CourseView();
        this.courseView.listenToBus();
    }

    routeConnections () {
        http.createServer( async (req, res) => {
            const courses = await this.courseView.getAllCourses();
            res.writeHead(200, {'Content-Type': 'application/json'});
            res.end(JSON.stringify({'status': 1, 'courses': courses}));
        }).listen(3001);
    }

}

module.exports = Router;
