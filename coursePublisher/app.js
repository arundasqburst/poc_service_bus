var createError = require('http-errors');
var express = require('express');

const DbConnector = require('./dbConnector');
const Route = require('./mainRoutes');
const Middleware = require('./setMiddlewares');

var app = express();

const dbConnector = new DbConnector();
dbConnector.connectDb();

const middleware = new Middleware(app);
middleware.setMiddlewares();

const route = new Route(app)
route.setRoutes();

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
