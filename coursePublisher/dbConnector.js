'use strict';
const mongoose = require('mongoose');

class MongoConnection {

    // Connect to mongo db
    
    constructor() {
        this.mongoUrl = 'mongodb://arundas:qburst123@ds261450.mlab.com:61450/course_app';
    }

    connectDb() {
        mongoose.connect(this.mongoUrl, (error, db) => {
            if (error) {
                console.log('Cannot establish database connection...');
            } else {
                console.log('Connection Established...');
            }
        });
    }

}

module.exports = MongoConnection;
