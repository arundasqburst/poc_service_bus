const express = require('express');
const Index = require('../views/index')
const router = express.Router();


/* GET home page. */
router.post('/', async (req, res, next) => {
  try{
    const indexPage = new Index()
    await indexPage.saveCourse(req.body);
    res.send({'status': 1, 'message': 'Course Created'});
  } catch(error) {
    console.log(error);
    res.send({'status': -1, 'error': 'Invalid Request', 'errorReason': error});
  }
});

router.get('/', async (req, res, next) => {
  try{
    const indexPage = new Index()
    const course = await indexPage.getCourse();
    res.send({'status': 1, 'course': course});
  } catch(error) {
    res.send({'status': -1, 'error': 'Invalid Request', 'errorReason': error});
  }
});

module.exports = router;
