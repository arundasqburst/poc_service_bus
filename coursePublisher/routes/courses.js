const express = require('express');
const Course = require('../views/course')
const router = express.Router();

router.get('/', async (req, res, next) => {
  try{
    const course = new Course()
    const courses = await course.getAllCourses();
    res.send({'status': 1, 'courses': courses});
  } catch(error) {
    res.send({'status': -1, 'error': 'Invalid Request', 'errorReason': error});
  }
});

module.exports = router;
