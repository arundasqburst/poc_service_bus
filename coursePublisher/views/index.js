const bus = require('servicebus').bus();

const Course = require('../models/course');

class IndexData {
    async saveCourse(reqData) {
        const course = await Course.create(reqData);
        bus.send('courseCreator', { course: reqData });
        bus.send('event.courseCreator', { course: reqData });
        return course;
    }

    async getCourse() {
        const course = await Course.findOne({'name': 'Name'});
        return course;
    }
}

module.exports = IndexData;
