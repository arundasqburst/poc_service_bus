const Course = require('../models/course');

class IndexData {

    async getAllCourses() {
        const courses = await Course.find();
        return courses;
    }
}

module.exports = IndexData;
