var express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');

class Middleware {

    constructor (app) {
        this.app = app;
    }

    setMiddlewares() {
        this.app.set('views', path.join(__dirname, 'views'));
        this.app.set('view engine', 'jade');

        this.app.use(logger('dev'));
        this.app.use(express.json());
        this.app.use(express.urlencoded({ extended: false }));
        this.app.use(cookieParser());
        this.app.use(express.static(path.join(__dirname, 'public')));
    }

}

module.exports = Middleware;
