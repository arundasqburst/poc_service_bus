const indexRouter = require('./routes/index');
const usersRouter = require('./routes/users');
const courseRouter = require('./routes/courses')

class SetBaseRoutes {

    constructor (app) {
        this.app = app;
    }

    setRoutes () {
        this.app.use('/', indexRouter);
        this.app.use('/users', usersRouter);
        this.app.use('/courses', courseRouter);
    }
}

module.exports = SetBaseRoutes;
